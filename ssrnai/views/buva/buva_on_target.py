from django.views.generic import DetailView
from ssrnai.models.conyza.conyza_on_targets import Conyza_On_Targets
from ssrnai.models.conyza.conyza_dsrna_information import Conyza_Dsrna_Information
from ssrnai.models.conyza.conyza_gene_information import Conyza_Gene_Information
from ssrnai.models import Database


class BuvaOnTarget(DetailView):
    template_name = 'buva/buva_on_target.html'
    context_object_name = 'ontargets'
    model = Conyza_On_Targets
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(BuvaOnTarget, self).get_context_data(**kwargs)
        context['ontargets'] = Conyza_On_Targets.objects.filter(dsrna=int(self.kwargs['pk']))
        ontarget = context['ontarget']
        context['dsRNA'] = Conyza_Dsrna_Information.objects.get(id=int(ontarget.dsrna_id))
        context['gene'] = Conyza_Gene_Information.objects.get(id=int(ontarget.gene_id))
        context['database'] = Database.objects.get(id=int(1))

        return context
