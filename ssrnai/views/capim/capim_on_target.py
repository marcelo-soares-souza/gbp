from django.views.generic import DetailView
from ssrnai.models.capim.capim_on_targets import Capim_On_Targets
from ssrnai.models.capim.capim_dsrna_information import Capim_Dsrna_Information
from ssrnai.models.capim.capim_gene_information import Capim_Gene_Information
from ssrnai.models import Database

class CapimOnTarget(DetailView):
    template_name = 'capim/capim_on_target.html'
    context_object_name = 'ontargets'
    model = Capim_On_Targets
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(CapimOnTarget, self).get_context_data(**kwargs)
        context['ontargets'] = Capim_On_Targets.objects.filter(dsrna=int(self.kwargs['pk']))
        ontarget = context['ontarget']
        context['dsRNA'] = Capim_Dsrna_Information.objects.get(id=int(ontarget.dsrna_id))
        context['gene'] = Capim_Gene_Information.objects.get(id=int(ontarget.gene_id))
        context['database'] = Database.objects.get(id=int(6))

        return context
