from django.views.generic import DetailView
from ssrnai.models.capim.capim_dsrna_information import Capim_Dsrna_Information
from ssrnai.models.capim.capim_gene_information import Capim_Gene_Information
from ssrnai.models import Database

class CapimSequence(DetailView):
    template_name = 'capim/capim_sequence.html'
    context_object_name = 'sequence'
    model = Capim_Dsrna_Information
    fields = '__all__'

    def get_context_data(self, **kwargs):
        # importing math module
        import math

        context = super(CapimSequence, self).get_context_data(**kwargs)
        context['dsrna'] = Capim_Dsrna_Information.objects.get(id=int(self.kwargs['pk']))
        dsrna = context['dsrna']
        new_seq = "" 
        chunks = math.ceil(len(dsrna.dsrna_seq)/80)
        for i in range(chunks):
            if(i==chunks-1):
                new_seq += dsrna.dsrna_seq[i*80:len(dsrna.dsrna_seq)]
                break
            new_seq += dsrna.dsrna_seq[i*80:80*(i+1)] + '\n'
        dsrna.dsrna_seq = new_seq

        context['gene'] = Capim_Gene_Information.objects.get(id=int(dsrna.gene_id))
        gene = context['gene']
        new_seq = "" 
        chunks = math.ceil(len(gene.cds_seq)/80)
        for i in range(chunks):
            if(i==chunks-1):
                new_seq += gene.cds_seq[i*80:len(gene.cds_seq)]
                break
            new_seq += gene.cds_seq[i*80:80*(i+1)] + '\n'
        gene.cds_seq = new_seq
        
        context['gene'] = gene
        context['database'] = Database.objects.get(id=int(6))

        return context
