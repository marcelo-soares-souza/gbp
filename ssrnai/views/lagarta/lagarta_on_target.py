from django.views.generic import DetailView
from ssrnai.models.lagarta.lagarta_on_targets import Lagarta_On_Targets
from ssrnai.models.lagarta.lagarta_dsrna_information import Lagarta_Dsrna_Information
from ssrnai.models.lagarta.lagarta_gene_information import Lagarta_Gene_Information
from ssrnai.models import Database

class LagartaOnTarget(DetailView):
    template_name = 'lagarta/lagarta_on_target.html'
    context_object_name = 'ontargets'
    model = Lagarta_On_Targets
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(LagartaOnTarget, self).get_context_data(**kwargs)
        context['ontargets'] = Lagarta_On_Targets.objects.filter(dsrna=int(self.kwargs['pk']))
        ontarget = context['ontarget']
        context['dsRNA'] = Lagarta_Dsrna_Information.objects.get(id=int(ontarget.dsrna_id))
        context['gene'] = Lagarta_Gene_Information.objects.get(id=int(ontarget.gene_id))
        context['database'] = Database.objects.get(id=int(4))

        return context
