from django.views.generic import DetailView
from ssrnai.models.percevejo.percevejo_on_targets import Percevejo_On_Targets
from ssrnai.models.percevejo.percevejo_dsrna_information import PercevejoDsrnaInformation
from ssrnai.models.percevejo.percevejo_gene_information import Percevejo_Gene_Information
from ssrnai.models import Database


class PercevejoOnTarget(DetailView):
    template_name = 'percevejo/percevejo_on_target.html'
    context_object_name = 'ontargets'
    model = Percevejo_On_Targets
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(PercevejoOnTarget, self).get_context_data(**kwargs)
        context['ontarget'] = Percevejo_On_Targets.objects.get(id=int(self.kwargs['pk']))
        ontarget = context['ontarget']
        context['dsRNA'] = PercevejoDsrnaInformation.objects.get(id=int(ontarget.dsrna_id))
        context['gene'] = Percevejo_Gene_Information.objects.get(id=int(ontarget.gene_id))
        context['database'] = Database.objects.get(id=int(5))

        return context
