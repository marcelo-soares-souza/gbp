from os import write
from django.db.models.fields import CharField
from django.shortcuts import render, get_object_or_404, render_to_response
from django.views.generic.base import View
from ssrnai.models import Organisms, Database, organisms
from django.views.generic import ListView
from django.db.models import Q
from ssrnai.models.percevejo.percevejo_dsrna_information import PercevejoDsrnaInformation
from ssrnai.models.percevejo.percevejo_gene_information import Percevejo_Gene_Information
from projeto.views.login import LoggedInMixin
from django.urls import reverse_lazy
from ssrnai.models.percevejo.percevejo_iscore import Percevejo_Iscore
from ssrnai.models.percevejo.percevejo_dicer import Percevejo_Dicer
from ssrnai.models.percevejo.percevejo_structure import Percevejo_Structure
from ssrnai.models.percevejo.percevejo_expression import Percevejo_Expression
from ssrnai.models.percevejo.percevejo_asiatico_expression import Percevejo_Asiatico_Expression
from ssrnai.models.percevejo.percevejo_on_targets import Percevejo_On_Targets
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect, request

class PercevejoResults(ListView):

    allowed_sort_fields = {'gene_name': {'default_direction': '', 'verbose_name': 'Gene_name'},
                           'dsrna_name': {'default_direction': '', 'verbose_name': 'Dsrna_name'}}

    default_sort_field = 'gene_name'

    template_name = 'percevejo/percevejo_results.html'
    paginate_by = 10
    context_object_name = 'page_obj'
    fields = '__all__'

    success_url = reverse_lazy('percevejo_results')

    def get(self, request):
        context = {}

        #gene = self.request.GET.get('gene', '')
        #genes = Percevejo_Gene_Information.objects.filter(gene_name__icontains=gene)
        #context['gene'] = gene

        organism = self.request.GET.get('organism', '0')
        gene = self.request.GET.get('gene', '')
        gene_function = self.request.GET.get('gene_function', '')
        go_function = self.request.GET.get('go_function', '')
        min_iscore = self.request.GET.get('min_iscore', '')
        max_iscore = self.request.GET.get('max_iscore', '')
        min_dicer = self.request.GET.get('min_dicer', '')
        max_dicer = self.request.GET.get('max_dicer', '')
        min_structure = self.request.GET.get('min_structure', '')
        max_structure = self.request.GET.get('max_structure', '')
        min_expression = self.request.GET.get('min_expression', '')
        max_expression = self.request.GET.get('max_expression', '')
        min_ontarget_number = self.request.GET.get('min_ontarget_number', '')
        max_ontarget_number = self.request.GET.get('max_ontarget_number', '')

        # organism search
        if organism == '0':
            neworganism = Organisms()
            neworganism.organism_name = 'Todos os organismos'
            context['organism'] = neworganism
        else:
            context['organism'] = Organisms.objects.get(id=int(organism))

        # gene search
        gene_list = []
        genes = []
        if gene == '*' or gene_function == '*' or go_function == '*':
            try:
                if organism != '0':
                    genes = Percevejo_Gene_Information.objects.filter(organism_id=int(organism))
                    #context['genes'] = genes
                else:
                    genes = Percevejo_Gene_Information.objects.all()
                    #context['genes'] = genes
            except ObjectDoesNotExist:
                genes = []
            
            for g in genes:
                gene_list.append(g)

        else:
            if not not gene:
                try:
                    if organism != '0':
                        genes = Percevejo_Gene_Information.objects.filter(gene_name__icontains=gene, organism_id=int(organism))
                        #context['genes'] = genes
                    else:
                        genes = Percevejo_Gene_Information.objects.filter(gene_name__icontains=gene)
                    #context['genes'] = genes
                except ObjectDoesNotExist:
                    genes = []

            for g in genes:
                gene_list.append(g)

            # gene function search
            genes = []
            nextgene = Percevejo_Gene_Information()
            if not not gene_function:
                try:
                    if organism != '0':
                        genes = Percevejo_Gene_Information.objects.filter(gene_description__icontains=gene_function, organism_id=int(organism))
                        #context['genes'] = genes
                    else:
                        genes = Percevejo_Gene_Information.objects.filter(gene_description__icontains=gene_function)
                        #context['genes'] = genes
                except ObjectDoesNotExist:
                    genes = []

            for g in genes:
                gene_list.append(g)

            genes = list(set(gene_list))
            gene_list = genes

            # GO function search
            genes = []
            nextgene = Percevejo_Gene_Information()
            if not not go_function:
                try:
                    if organism != '0':
                        genes = Percevejo_Gene_Information.objects.filter(gene_ontology_blastx__icontains=go_function, organism_id=int(organism))
                    #context['gene_ontology_blastx'] = newgene
                    else:
                        genes = Percevejo_Gene_Information.objects.filter(gene_ontology_blastx__icontains=go_function)

                except ObjectDoesNotExist:
                    genes = []

            for g in genes:
                gene_list.append(g)

            context['genes'] = gene_list

        # dsRNA search
        ds_list = []
        result_list = []

        def expressionSearch(g):

            expression = []
            
            if(g.organism_id == 13):
                try:
                    expression = Percevejo_Asiatico_Expression.objects.filter(gene=int(g.id))
                except ObjectDoesNotExist:
                    expression = []
            else:
                try:
                    expression = Percevejo_Expression.objects.filter(gene=int(g.id))
                except ObjectDoesNotExist:
                    expression = []
            
            return(expression)

        def databaseSearches(ds):

            iscore = []
            dicer = []
            estrutura = []
            ontarget = []

            try:
                iscore = Percevejo_Iscore.objects.filter(dsrna=int(ds.id))
            except ObjectDoesNotExist:
                iscore = []

            try:
                dicer = Percevejo_Dicer.objects.filter(dsrna=int(ds.id))
            except ObjectDoesNotExist:
                dicer = []

                        # if(len(dicer)>1):
                        #    dicer = dicer[0]

            try:
                estrutura = Percevejo_Structure.objects.filter(dsrna=int(ds.id))
            except ObjectDoesNotExist:
                estrutura = []

                        # if(len(estrutura)>1):
                        #    estrutura = estrutura[0]
            try:
                ontarget = Percevejo_On_Targets.objects.filter(dsrna=int(ds.id))
            except ObjectDoesNotExist:
                ontarget = []
            
            return[iscore,dicer,estrutura,ontarget]

        # cria uma lista de resultado.
        for g in gene_list:
            
            dsRNAs = []
            try:
                dsRNAs = PercevejoDsrnaInformation.objects.filter(gene=int(g.id))
                #context['gene_ontology_blastx'] = newgene
            except ObjectDoesNotExist:
                dsRNAs = []

            result = []
            if not not dsRNAs:
                for ds in dsRNAs:
                    iscore = []
                    dicer = []
                    estrutura = []
                    ontarget = []
                    expression = []
                    # if not not min_iscore or not not max_iscore or not not min_dicer or not not max_dicer or not not min_ontarget_number or not not max_ontarget_number:
                    #     if not not min_iscore and not not max_iscore:
                    #         #print("1")
                    #         try:
                    #             iscore = Percevejo_Iscore.objects.filter((Q(dsrna=int(ds.id))), (Q(max_iscore__lte=min_iscore) | Q(min_iscore__gte=max_iscore)))
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not iscore:
                    #             continue

                    #     elif not not min_iscore:
                    #         #print("2")
                    #         try:
                    #             iscore = Percevejo_Iscore.objects.filter(dsrna=int(ds.id), max_iscore__gte=min_iscore)
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not iscore:
                    #             continue

                    #     elif not not max_iscore:
                    #         print("3")
                    #         try:
                    #             iscore = Percevejo_Iscore.objects.filter(dsrna=int(ds.id), max_iscore__lte=max_iscore)
                    #             #userprofile__level__gte=0
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not iscore:
                    #             continue
        
                    #     if not not min_dicer and not not max_dicer:
                    #         print("4")
                    #         try:
                    #             dicer = Percevejo_Dicer.objects.filter(dsrna=int(ds.id), sirna_number__gte=min_dicer, sirna_number__lte=max_dicer)
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not dicer:
                    #             continue

                    #     elif not not min_dicer:
                    #         print("5")
                    #         try:
                    #             dicer = Percevejo_Dicer.objects.filter(dsrna=int(ds.id), sirna_number__gte=min_dicer)
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not dicer:
                    #             continue
                        
                    #     elif not not max_dicer:
                    #         print("6")
                    #         try:
                    #             dicer = Percevejo_Dicer.objects.filter(dsrna=int(ds.id), sirna_number__lte=max_dicer)
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not dicer:
                    #             continue
                        
                    #     if not not min_ontarget_number and not not max_ontarget_number:
                    #         print("7")
                    #         try:
                    #             ontarget = Percevejo_On_Targets.objects.filter(dsrna=int(ds.id), number_ontarget__gte=min_ontarget_number, number_ontarget__lte=max_ontarget_number)
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not ontarget:
                    #             continue

                    #     elif not not min_ontarget_number:
                    #         print("8")
                    #         try:
                    #             ontarget = Percevejo_On_Targets.objects.filter(dsrna=int(ds.id), number_ontarget__gte=min_ontarget_number)
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not ontarget:
                    #             continue
                        
                    #     elif not not max_ontarget_number:
                    #         print("9")
                    #         try:
                    #             ontarget = Percevejo_On_Targets.objects.filter(dsrna=int(ds.id), number_ontarget__lte=max_ontarget_number)
                    #         except ObjectDoesNotExist:
                    #             continue
                    #         if not ontarget:
                    #             continue
                        
                    #     expression = expressionSearch(g)
                    #     searches = databaseSearches(ds)
                    #     #iscore,dicer,estrutura,ontarget
                    #     iscore = searches[0]
                    #     dicer = searches[1]
                    #     estrutura = searches[2]
                    #     ontarget = searches[3]
                        
                    # else:
                    expression = expressionSearch(g)
                    searches = databaseSearches(ds)
                        #iscore,dicer,estrutura,ontarget
                    iscore = searches[0]
                    dicer = searches[1]
                    estrutura = searches[2]
                    ontarget = searches[3]

                    result.append(g.id)  # 0
                    result.append(g.gene_name)  # 1

                    # importing math module
                    import math
                    new_seq = ""
                    chunks = math.ceil(len(g.gene_description)/80)
                    for i in range(chunks):
                        if(i == chunks-1):
                            new_seq += g.gene_description[i * 80:len(g.gene_description)]
                        else:
                            new_seq += g.gene_description[i*80:80*(i+1)] + '\n'

                    result.append(new_seq)  # 2
                    # result.append(g.gene_description) #2
                    result.append(g.gene_ontology_blastx)  # 3
                    result.append(g.length)  # 4

                    if not not expression:
                        result.append(expression[0].id)  # 5
                    else:
                        result.append('-')

                    result.append(ds.dsrna_name)  # 6
                    localizacao = (str(ds.start) + '-' +
                                   str(ds.stop))  # NOT A RESULT!!!
                    result.append(localizacao)  # 7
                    result.append(ds.id)  # 8

                    if not not iscore:
                        result.append(iscore[0].id)  # 9
                        result.append(iscore[0].mean_dsir)  # 10
                        result.append(iscore[0].mean_iscore)  # 11
                        result.append(iscore[0].mean_sbiopredsi)  # 12
                    else:
                        result.append("-")
                        result.append("-")
                        result.append("-")
                        result.append("-")

                    if not not dicer:
                        result.append(dicer[0].id)  # 13
                        result.append(dicer[0].sirna_number)  # 14
                        result.append(dicer[0].coverage)  # 15
                    else:
                        result.append("-")
                        result.append("-")
                        result.append("-")

                    if not not estrutura:
                        result.append(estrutura[0].id)  # 16
                        result.append(estrutura[0].classification)  # 17
                    else:
                        result.append("-")
                        result.append("-")

                    if not not ontarget:
                        result.append(ontarget[0].id)  # 18
                    else:
                        result.append("-")

                    result.append("-")  # 19
                    result.append(g.organism_id)  # 20
                    result_list.append(result)
                    result = []
                    ds_list.append(ds)
            else:
                #if not min_iscore and not max_iscore and not min_dicer and not max_dicer and not min_ontarget_number and not max_ontarget_number:

                    expression = expressionSearch(g)

                    result.append(g.id)  # 0
                    result.append(g.gene_name)  # 1
                    result.append(g.gene_description)  # 2
                    result.append(g.gene_ontology_blastx)  # 3
                    result.append(g.length)  # 4

                    if not not expression:
                        result.append(expression[0].id)  # 5
                    else:
                        result.append('-')

                    result.append("-")  # 6
                    result.append("-")  # 7
                    result.append("-")  # 8
                    result.append("-")  # 9
                    result.append("-")  # 10
                    result.append("-")  # 11
                    result.append("-")  # 12
                    result.append("-")  # 13
                    result.append("-")  # 14
                    result.append("-")  # 15
                    result.append("-")  # 16
                    result.append("-")  # 17
                    result.append("-")  # 18
                    result.append("-")  # 19
                    result.append(g.organism_id)  # 20
                    result_list.append(result)

        # open the file in the write mode
        import csv
        f = open('media/ssrnai/Percevejos/FilesToDownload/results.csv', 'w')
        writer = csv.writer(f)
        header = "Gene", "Função gênica", "GO", "Tamanho CDS", "dsRNA", "Localização", "Média DSIR", "Média i-Score", "Média s-Biopredsi", "Número DICER", "Tamanho DICER", "Classificação estrutura"
        writer.writerow(header)
        for result in result_list:
            r = result[1], result[2], result[3], result[4], result[6], result[7], result[
                10], result[11], result[12], result[14], result[15], result[17]
            writer.writerow(r)

        # close the file
        f.close()

        page = self.request.GET.get('page', 1)

        paginator = Paginator(result_list, 10)

        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        return render(request, 'percevejo/percevejo_results.html', {'results': results})    